/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { HelpscoutService } from './helpscout.service';

describe('Service: Helpscout', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HelpscoutService]
    });
  });

  it('should ...', inject([HelpscoutService], (service: HelpscoutService) => {
    expect(service).toBeTruthy();
  }));
});