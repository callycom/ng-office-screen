/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DatumprikkerArchiveService } from './datumprikker-archive.service';

describe('Service: DatumprikkerArchive', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DatumprikkerArchiveService]
    });
  });

  it('should ...', inject([DatumprikkerArchiveService], (service: DatumprikkerArchiveService) => {
    expect(service).toBeTruthy();
  }));
});