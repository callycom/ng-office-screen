import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Settings } from '../utilities/settings';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class FabricService {

    constructor(private http: Http) { }

    public getOnlineUsers(): Observable<any> {
        return this.http.get(Settings.base +  Settings.baseUrlFabric + 'online')
            .map(response => response.json())
            .catch(this.handleError);
    }

    public getAppUsers(): Observable<any> {
        return this.http.get(Settings.base +  Settings.baseUrlFabric + 'app?period=week')
            .map(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        return error;
    }
}
