import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Settings } from '../utilities/settings';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class HelpScoutService {

    constructor(private http: Http) { }

    public getHelpdeskStats(): Observable<any> {
        return this.http.get(Settings.base +  Settings.baseUrlHelpscout + 'helpdeskstats')
            .map(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        return error;
    }
}
