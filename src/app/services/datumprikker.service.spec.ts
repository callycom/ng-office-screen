/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DatumprikkerService } from './datumprikker.service';

describe('Service: Datumprikker', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DatumprikkerService]
    });
  });

  it('should ...', inject([DatumprikkerService], (service: DatumprikkerService) => {
    expect(service).toBeTruthy();
  }));
});