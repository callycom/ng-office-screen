import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Settings } from '../utilities/settings';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class DatumprikkerArchiveService {

    constructor(private http: Http) { }

    public getAppointmentGraphs(): Observable<any> {
        return this.http.get(Settings.base + Settings.baseUrlDatumprikkerArchive + 'events?period=month&groupby=day')
            .map(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        return error;
    }
}
