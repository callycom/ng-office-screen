import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Settings } from '../utilities/settings';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class DatumprikkerService {

    constructor(private http: Http) { }

    public getAllTimeStatistics(): Observable<any> {
        return this.http.get(Settings.base +  Settings.baseUrlDatumprikker + 'stats?period=all_time')
            .map(response => response.json())
            .catch(this.handleError);
    }

    public getTodaysStatistics(): Observable<any> {
        return this.http.get(Settings.base +  Settings.baseUrlDatumprikker + 'stats?period=day')
            .map(response => response.json())
            .catch(this.handleError);
    }

    public getCategoryDivision(): Observable<any> {
        return this.http.get(Settings.base +  Settings.baseUrlDatumprikker + 'categories?period=day')
            .map(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        return error;
    }
}
