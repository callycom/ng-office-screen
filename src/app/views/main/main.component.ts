import { Observable } from 'rxjs/Observable';
import { Component, OnInit } from '@angular/core';
import { trigger, transition, style, animate, state, keyframes } from '@angular/animations';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  animations: [
    trigger('easeInOut', [
      transition(':enter', [
        style({
          opacity: 0
        }),
        animate('1s ease-in-out', style({
          opacity: 1
        }))
      ]),
      transition(':leave', [
        style({
          opacity: 1
        }),
        animate('1s ease-in-out', style({
          opacity: 0
        }))
      ])
    ])
  ]
})
export class MainComponent implements OnInit {
  public overview = true;
  public overviewMoreInfo = false;

  constructor() { }

  ngOnInit() {
    /*setInterval(() => {
      this.changeScreen();
    }, 30000);*/
  }

  public changeScreen(): void {
    if (this.overview === true) {
      this.overview = false;
      this.overviewMoreInfo = true;
    } else {
      this.overview = true;
      this.overviewMoreInfo = false;
    }
  }

}
