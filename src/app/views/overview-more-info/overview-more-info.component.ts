import { BaseChartDirective } from 'ng2-charts';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DatumprikkerArchiveService } from '../../services/datumprikker-archive.service';
import { DatumprikkerService } from '../../services/datumprikker.service';
import { DatePipe, DecimalPipe } from '@angular/common';
import { FabricService } from '../../services/fabric.service';
import { HelpScoutService } from '../../services/helpscout.service';
import { Settings } from '../../utilities/settings';

@Component({
  selector: 'app-overview-more-info',
  templateUrl: './overview-more-info.component.html',
  styleUrls: ['./overview-more-info.component.css']
})
export class OverviewMoreInfoComponent implements OnInit {
  @ViewChild(BaseChartDirective) chart: BaseChartDirective;
  public amountOfAppointments = 0;
  public amountOfUsers = 0;
  public sendEmails = 0;
  public amountResponded = 0;
  public amountOfUsersOnline = 0;
  public amountSend = 102;
  public amountSolved = 65;
  public amountSolvedAfterResponse = 68;
  public userList = [];
  public currentDate = this.datePipe.transform(new Date(), 'dd-MM-yyyy');
  public trophyListSrc = ['../../../assets/img/gold.svg', '../../../assets/img/silver.svg', '../../../assets/img/bronze.svg'];
  public barChartLegend = true;
  public barChartType = 'bar';
  public barChartOptions: any = Settings.barChartConfig;
  public barChartOptionsCategories: any = Settings.barChartConfigCategories;
  public barChartDataCategories: Array<any> = [
    { data: [], label: '' },
    { data: [], label: '' },
    { data: [], label: '' },
    { data: [], label: '' },
    { data: [], label: '' },
    { data: [], label: '' },
    { data: [], label: '' }
  ];
  public barChartLabelsCategories: Array<any> = [];
  public barChartColorsCategories = Settings.barChartColorsCategories;
  public doughnutChartColors = Settings.dougnutChartColors;
  public doughnutChartOptions = Settings.dougnutChartConfig;
  public doughnutChartLabels = [];
  public doughnutChartData = [];
  public doughnutChartType = 'doughnut';

  private finalAmountOfUsersOnline: number;
  private finalAmountOfAppointments: number;
  private finalAmountOfUsers: number;
  private finalAmountResponded: number;
  private finalSendEmails: number;
  private interval = window.setInterval(this.callUpdate.bind(this), 10);

  constructor(public decimalPipe: DecimalPipe, private datumprikkerService: DatumprikkerService,
    private datumprikkerArchiveService: DatumprikkerArchiveService, private fabricService: FabricService,
    private helpscoutService: HelpScoutService, private datePipe: DatePipe) {
    this.interval = window.setInterval(this.callUpdate.bind(this), 10);
  }

  ngOnInit() {
    this.setActiveUsers();
    this.setTodayStatistics();
    this.setCategories();
  }

  private setCategories(): void {
    this.datumprikkerService.getCategoryDivision()
      .subscribe(response => {
        this.barChartDataCategories = [];
        response.result.graphs.forEach(point => {
          this.barChartDataCategories.push({
            data: [point.data],
            label: point.labels
          });
          this.barChartLabelsCategories = ['Today'];
        });
      }, error => {

      });
  }

  private setActiveUsers(): void {
    this.fabricService.getOnlineUsers()
      .subscribe(response => {
        const stat = response.result;
        const android = stat.android_percentage;
        const apple = stat.apple_percentage;
        const desktop = stat.desktop_percentage;
        this.doughnutChartData = [];
        this.doughnutChartLabels = [];

        this.finalAmountOfUsersOnline = stat.total;
        this.doughnutChartData.push(desktop);
        this.doughnutChartData.push(android);
        this.doughnutChartData.push(apple);
        setTimeout(() => this.doughnutChartLabels = ['Desktop: ' + desktop, 'Android: ' + android, 'Apple: ' + apple]);
      }, error => {

      });
  }

  private setTodayStatistics(): void {
    this.datumprikkerService.getTodaysStatistics()
      .subscribe(response => {
        const stat = response.result;

        this.finalAmountOfAppointments = stat.events;
        this.finalAmountOfUsers = stat.users;
        this.finalAmountResponded = stat.availability;
        this.finalSendEmails = stat.emails;
      }, error => {

      });
  }

  private callUpdate(): void {
    if (this.amountOfUsersOnline === this.finalAmountOfUsersOnline &&
      this.amountOfAppointments === this.finalAmountOfAppointments &&
      this.amountOfUsers === this.finalAmountOfUsers &&
      this.amountResponded === this.finalAmountResponded &&
      this.sendEmails === this.finalSendEmails) {
      window.clearInterval(this.interval);
    } else {
      this.updateCounter();
    }
  }

  private updateCounter(): void {
    if (this.amountOfUsersOnline < this.finalAmountOfUsersOnline) {
      this.amountOfUsersOnline += 100;
    } else {
      this.amountOfUsersOnline = this.finalAmountOfUsersOnline;
    }
    if (this.amountOfAppointments < this.finalAmountOfAppointments) {
      this.amountOfAppointments += 250;
    } else {
      this.amountOfAppointments = this.finalAmountOfAppointments;
    }
    if (this.amountOfUsers < this.finalAmountOfUsers) {
      this.amountOfUsers += 25000;
    } else {
      this.amountOfUsers = this.finalAmountOfUsers;
    }
    if (this.amountResponded < this.finalAmountResponded) {
      this.amountResponded += 25000;
    } else {
      this.amountResponded = this.finalAmountResponded;
    }
    if (this.sendEmails < this.finalSendEmails) {
      this.sendEmails += 25000;
    } else {
      this.sendEmails = this.finalSendEmails;
    }
  }

}
