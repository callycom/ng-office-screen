import { DateFormatter } from '@angular/common/src/pipes/deprecated/intl';
import { BaseChartDirective } from 'ng2-charts';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DatumprikkerArchiveService } from '../../services/datumprikker-archive.service';
import { DatumprikkerService } from '../../services/datumprikker.service';
import { DecimalPipe, DatePipe } from '@angular/common';
import { FabricService } from '../../services/fabric.service';
import { HelpScoutService } from '../../services/helpscout.service';
import { Settings } from '../../utilities/settings';


@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {
  @ViewChild(BaseChartDirective) chart: BaseChartDirective;
  public amountOfAppointments = 0;
  public amountOfAppointmentsLabel;
  public amountOfUsers = 0;
  public amountOfUsersLabel;
  public amountOfInvitedUsers = 0;
  public amountOfInvitedUsersLabel;
  public amountOfProposedDates = 0;
  public amountOfProposedDatesLabel;
  public amountOfNewTickets = 0;
  public amountOfUsersOnline = 0;
  public amountOfUsersOnlineLabel;
  public appUsersLabel;
  public userList = [];
  public trophyListSrc = ['../../../assets/img/gold.svg', '../../../assets/img/silver.svg', '../../../assets/img/bronze.svg'];
  public lineChartData: Array<any> = [
    { data: [], label: '' },
    { data: [], label: '' }
  ];
  public lineChartLabels: Array<any> = [];
  public lineChartOptions: any = Settings.lineChartConfig;
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartColors = Settings.lineChartColors;
  public barChartLegend = true;
  public barChartType = 'bar';
  public barChartOptions: any = Settings.barChartConfig;
  public barChartOptionsCategories: any = Settings.barChartConfigCategories;
  public barChartDataCategories: Array<any> = [
    { data: [], label: '' },
    { data: [], label: '' },
    { data: [], label: '' },
    { data: [], label: '' },
    { data: [], label: '' },
    { data: [], label: '' },
    { data: [], label: '' },
    { data: [], label: '' },
  ];
  public barChartLabelsCategories: Array<any> = [];
  public barChartColorsCategories = Settings.barChartColorsCategories;
  public barChartDataUsers: Array<any> = [
    { data: [], label: '' },
    { data: [], label: '' }
  ];
  public barChartLabelsUsers: Array<any> = [];
  public barChartColorsUsers = Settings.barChartColorsUsers;
  public categoriesLabel;
  public doughnutChartOptions = Settings.dougnutChartConfig;
  public doughnutChartLabels = [];
  public doughnutChartData = [];
  public doughnutChartType = 'doughnut';
  public doughnutChartColors = Settings.dougnutChartColors;
  public sendGrowth: number;
  public solvedGrowth: number;
  public solvedGrowthResponse: number;
  public statsTodayLabel;
  public totalEventsLabel;
  public day = this.datePipe.transform(new Date(), 'dd-MM-yyyy');

  private finalAmountOfUsersOnline: number;
  private finalAmountOfAppointments: number;
  private finalAmountOfUsers: number;
  private finalAmountInvitedUsers: number;
  private finalAmountOfProposedDates: number;
  private finalAmountOfNewTickets: number;
  private interval = window.setInterval(this.callUpdate.bind(this), 50);

  constructor(public decimalPipe: DecimalPipe,
    public datePipe: DatePipe,
    private datumprikkerService: DatumprikkerService,
    private datumprikkerArchiveService: DatumprikkerArchiveService,
    private fabricService: FabricService,
    private helpscoutService: HelpScoutService) {
    setInterval(this.updateCounter.bind(this), 1);
  }

  ngOnInit() {
    this.fetchData();
    this.setTotalAmountOfEvents();
    setInterval(() => {
      this.resetValues();
      this.fetchData();
    }, 30000);
    setInterval(() => {
      this.setActiveUsers();
    }, 5000);
  }

  private fetchData(): void {
    // this.setHelpScoutStatistics();
    this.setTodayStatistics();
    this.setActiveUsers();
    this.setAppUsers();
    this.setCategoriesChart();
  }

  private resetValues(): void {
    this.barChartDataUsers = [
      { data: [], label: '' },
      { data: [], label: '' }
    ];
    this.barChartLabelsUsers = [];
    this.barChartDataCategories = [
      { data: [], label: '' }
    ];
    this.barChartLabelsCategories = [];
  }

  private setAppUsers(): void {
    this.fabricService.getAppUsers()
      .subscribe(response => {
        const currentYear = [];
        const pastYear = [];
        const year = (new Date()).getFullYear();
        this.barChartDataUsers = [];
        this.appUsersLabel = response.label;
        response.sets[1].points.forEach(point => {
          currentYear.push(point.value);
          this.barChartLabelsUsers.push(point.label);
        });
        response.sets[0].points.forEach(point => {
          pastYear.push(point.value);
        });
        this.barChartDataUsers.push({
          data: currentYear,
          label: response.sets[1].label
        });
        this.barChartDataUsers.push({
          data: pastYear,
          label: response.sets[0].label
        });
      }, error => {

      });
  }

  private setCategoriesChart(): void {
    this.datumprikkerService.getCategoryDivision()
      .subscribe(response => {
        this.barChartDataCategories = [];
        this.categoriesLabel = response.label;
        response.sets[0].points.forEach(point => {
          this.barChartDataCategories.push({
            data: [point.value],
            label: point.label
          });
        });
      }, error => {

      });
  }

  private setTotalAmountOfEvents(): void {
    this.datumprikkerArchiveService.getAppointmentGraphs()
      .subscribe(response => {
        const currentYear = [];
        const pastYear = [];
        const year = (new Date()).getFullYear();
        this.lineChartData = [];
        this.totalEventsLabel = response.label;
        response.sets[0].points.forEach(point => {
          currentYear.push(point.value);
          this.lineChartLabels.push(point.label.substring(0, 3));
        });
        response.sets[1].points.forEach(point => {
          pastYear.push(point.value);
        });
        this.lineChartData.push({
          data: currentYear,
          label: year
        });
        this.lineChartData.push({
          data: pastYear,
          label: year - 1
        });
      }, error => {

      });
  }

  private setActiveUsers(): void {
    this.fabricService.getOnlineUsers()
      .subscribe(response => {
        const stat = response.sets[0].points;
        const android = stat[1].value;
        const apple = stat[0].value;
        const desktop = stat[2].value;
        this.doughnutChartData = [];
        this.doughnutChartLabels = [];
        this.amountOfUsersOnlineLabel = response.label;
        this.finalAmountOfUsersOnline = stat[3].value;
        this.doughnutChartData.push(desktop);
        this.doughnutChartData.push(android);
        this.doughnutChartData.push(apple);
        // tslint:disable-next-line:max-line-length
        setTimeout(() => this.doughnutChartLabels = [stat[2].label + ': '  + desktop, stat[1].label + ': ' + android, stat[0].label + ': ' + apple]);
      }, error => {

      });
  }

  private setTodayStatistics(): void {
    this.datumprikkerService.getTodaysStatistics()
      .subscribe(response => {
        const stat = response;
        this.statsTodayLabel = stat.label;
        this.finalAmountOfAppointments = stat.eventsValue;
        this.finalAmountInvitedUsers = stat.participantsValue;
        this.finalAmountOfProposedDates = stat.datesValue;
        this.finalAmountOfUsers = stat.usersValue;

        this.amountOfAppointmentsLabel = stat.eventsLabel;
        this.amountOfUsersLabel = stat.usersLabel;
        this.amountOfInvitedUsersLabel = stat.participantsLabel;
        this.amountOfProposedDatesLabel = stat.datesLabel;
      }, error => {

      });
  }

  private setHelpScoutStatistics(): void {
    this.helpscoutService.getHelpdeskStats()
      .subscribe(response => {
        const stat = response;
        this.finalAmountOfNewTickets = stat.new_tickets;
        this.sendGrowth = stat.week_replied_difference;
        this.solvedGrowth = stat.week_solved_difference;
        this.solvedGrowthResponse = stat.week_solved_single_reply_difference;
        this.userList = stat.user_stats;
      }, error => {

      });
  }

  private callUpdate(): void {
    if (this.amountOfUsersOnline === this.finalAmountOfUsersOnline &&
      this.amountOfAppointments === this.finalAmountOfAppointments &&
      this.amountOfUsers === this.finalAmountOfUsers &&
      this.amountOfInvitedUsers === this.finalAmountInvitedUsers &&
      this.amountOfProposedDates === this.finalAmountOfProposedDates &&
      this.amountOfNewTickets === this.finalAmountOfNewTickets) {
      window.clearInterval(this.interval);
    } else {
      this.updateCounter();
    }
  }

  private updateCounter(): void {
    if (this.amountOfUsersOnline < this.finalAmountOfUsersOnline) {
      this.amountOfUsersOnline += 500;
    } else {
      this.amountOfUsersOnline = this.finalAmountOfUsersOnline;
    }
    if (this.amountOfAppointments < this.finalAmountOfAppointments) {
      this.amountOfAppointments += 500;
    } else {
      this.amountOfAppointments = this.finalAmountOfAppointments;
    }
    if (this.amountOfUsers < this.finalAmountOfUsers) {
      this.amountOfUsers += 500;
    } else {
      this.amountOfUsers = this.finalAmountOfUsers;
    }
    if (this.amountOfInvitedUsers < this.finalAmountInvitedUsers) {
      this.amountOfInvitedUsers += 1000;
    } else {
      this.amountOfInvitedUsers = this.finalAmountInvitedUsers;
    }
    if (this.amountOfProposedDates < this.finalAmountOfProposedDates) {
      this.amountOfProposedDates += 1000;
    } else {
      this.amountOfProposedDates = this.finalAmountOfProposedDates;
    }
    if (this.amountOfNewTickets < this.finalAmountOfNewTickets) {
      this.amountOfNewTickets += 1;
    } else {
      this.amountOfNewTickets = this.finalAmountOfNewTickets;
    }
  }

}
