import { AppRoutingModule } from './app.routing';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { CommonModule, DatePipe, DecimalPipe, registerLocaleData } from '@angular/common';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { LOCALE_ID } from '@angular/core';
import localeNl from '@angular/common/locales/nl';

import { DatumprikkerArchiveService } from './services/datumprikker-archive.service';
import { DatumprikkerService } from './services/datumprikker.service';
import { FabricService } from './services/fabric.service';
import { HelpScoutService } from './services/helpscout.service';

import { AppComponent } from './app.component';
import { MainComponent } from './views/main/main.component';
import { OverviewComponent } from './views/overview/overview.component';
import { OverviewMoreInfoComponent } from './views/overview-more-info/overview-more-info.component';

registerLocaleData(localeNl);

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    OverviewComponent,
    OverviewMoreInfoComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    ChartsModule,
    HttpModule,
    NoopAnimationsModule
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'nl-NL'
    },
    DatumprikkerArchiveService,
    DatumprikkerService,
    DatePipe,
    DecimalPipe,
    FabricService,
    HelpScoutService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
