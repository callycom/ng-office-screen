'use strict';
export const Settings = Object.freeze({
    base: 'http://localhost:53938',
    baseUrlDatumprikker: '/api/datumprikker/',
    baseUrlDatumprikkerArchive: '/api/archive/',
    baseUrlHelpscout: '/api/helpscout/',
    baseUrlFabric: '/api/users/',
    lineChartConfig: {
        responsive: true,
        maintainAspectRatio: true,
        legend: {
            labels: {
                fontFamily: 'Circular Pro Book',
                fontSize: 18
            }
        },
        scales: {
            yAxes: [{
                display: true,
                ticks: {
                    fontColor: '#303030',
                    beginAtZero: true,
                    fontFamily: 'Circular Pro Book',
                    fontSize: 22
                }
            }],
            xAxes: [{
                display: true,
                ticks: {
                    fontColor: '#303030',
                    fontFamily: 'Circular Pro Book',
                    fontSize: 22
                }
            }]
        },
        elements: {
            point: {
                radius: 4,
                borderWidth: 4
            }
        }
    },
    lineChartColors: [
        { // main
            backgroundColor: 'rgba(165, 233, 187, 0.6)',
            borderColor: '#45B169',
            pointBackgroundColor: '#45B169',
            pointBorderColor: '#45B169',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#45B169'
        },
        { // reflection
            backgroundColor: 'rgba(165, 233, 187, 0.0)',
            borderColor: '#CCCCCC',
            pointBackgroundColor: '#CCCCCC',
            pointBorderColor: '#CCCCCC',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
        }
    ],
    dougnutChartConfig: {
        responsive: true,
        animation: false,
        maintainAspectRatio: true,
        legend: {
            labels: {
                fontFamily: 'Circular Pro Book',
                fontSize: 18
            }
        }
    },
    dougnutChartColors: [
        {
            backgroundColor: [
                '#179FE8',
                '#CDDC39',
                '#E64A4A',
                '#F5A623',
                '#00838F',
                '#47D074'
            ]
        }
    ],
    barChartColorsCategories: [
        {
            backgroundColor: '#179FE8',
            borderColor: '#179FE8',
            pointBackgroundColor: '#45B169',
            pointBorderColor: '#45B169',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#45B169'
        },
        {
            backgroundColor: '#1386C4',
            borderColor: '#1386C4',
            pointBackgroundColor: '#45B169',
            pointBorderColor: '#45B169',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#45B169'
        },
        {
            backgroundColor: '#E64A4A',
            borderColor: '#E64A4A',
            pointBackgroundColor: '#45B169',
            pointBorderColor: '#45B169',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#45B169'
        },
        {
            backgroundColor: '#CC4242',
            borderColor: '#CC4242',
            pointBackgroundColor: '#45B169',
            pointBorderColor: '#45B169',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#45B169'
        },
        {
            backgroundColor: '#CDDC39',
            borderColor: '#CDDC39',
            pointBackgroundColor: '#45B169',
            pointBorderColor: '#45B169',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#45B169'
        },
        {
            backgroundColor: '#F5A623',
            borderColor: '#F5A623',
            pointBackgroundColor: '#45B169',
            pointBorderColor: '#45B169',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#45B169'
        },
        {
            backgroundColor: '#47D074',
            borderColor: '#47D074',
            pointBackgroundColor: '#45B169',
            pointBorderColor: '#45B169',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#45B169'
        },
        {
            backgroundColor: '#46B26A',
            borderColor: '#46B26A',
            pointBackgroundColor: '#45B169',
            pointBorderColor: '#45B169',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#45B169'
        }
    ],
    barChartConfig: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            labels: {
                fontFamily: 'Circular Pro Book',
                fontSize: 18
            }
        },
        scales: {
            yAxes: [{
                display: true,
                stacked: true,
                ticks: {
                    fontColor: '#303030',
                    beginAtZero: true,
                    fontFamily: 'Circular Pro Book',
                    fontSize: 18,
                }
            }],
            xAxes: [{
                display: true,
                stacked: true,
                ticks: {
                    fontColor: '#303030',
                    fontFamily: 'Circular Pro Book',
                    fontSize: 18
                }
            }]
        }
    },
    barChartConfigCategories: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            labels: {
                fontFamily: 'Circular Pro Book',
                fontSize: 18
            }
        },
        scales: {
            yAxes: [{
                display: true,
                ticks: {
                    fontColor: '#303030',
                    beginAtZero: true,
                    fontFamily: 'Circular Pro Book',
                    fontSize: 18,
                }
            }],
            xAxes: [{
                display: true,
                ticks: {
                    fontColor: '#303030',
                    fontFamily: 'Circular Pro Book',
                    fontSize: 18
                }
            }]
        }
    },
    barChartColorsTotal: [
        { // main
            backgroundColor: '#179FE8',
            borderColor: '#179FE8',
            pointBackgroundColor: '#45B169',
            pointBorderColor: '#45B169',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#45B169'
        }
    ],
    barChartColorsUsers: [
        { // ANDROID
            backgroundColor: '#47D074',
            borderColor: '#47D074',
            pointBackgroundColor: '#45B169',
            pointBorderColor: '#45B169',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#45B169'
        },
        { // IOS
            backgroundColor: '#179FE8',
            borderColor: '#179FE8',
            pointBackgroundColor: '#45B169',
            pointBorderColor: '#45B169',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#45B169'
        }
    ]
});
